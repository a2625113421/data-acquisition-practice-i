import re
import requests
import urllib.request

def get_html_request(url):
    try:
        headers = {"User-Agent": "Mozilla/5.0 (Windows; U; Windows NT 6.0 x64; en-US; rv:1.9pre) Gecko/2008072421 Minefield/3.0.2pre"}
        r = requests.get(url, timeout = 30, headers=headers)
        r.raise_for_status()
        r.encoding = r.apparent_encoding
    except:
        return "产生异常"
    return r.text

def get_html_urllib(url):
    try:
        headers = {
            "User-Agent": "Mozilla/5.0 (Windows; U; Windows NT 6.0 x64; en-US; rv:1.9pre) Gecko/2008072421 Minefield/3.0.2pre"}
        req = urllib.request.Request(url, headers=headers)
        data = urllib.request.urlopen(req)
        data = data.read()
        data = data.decode()
        return data
    except Exception as err:
        print(err)

def download_jps(html):
    reg = '<img src="/([a-zA-z]+[^\s"]*)'
    imagelist = re.compile(reg).findall(html)
    i = 1
    for image in imagelist:
        imageurl = "http://news.fzu.edu.cn/" + image
        response = requests.get(imageurl)
        img = response.content
        with open("./picture/第" + str(i) +"张图片.jpg", "wb" ) as f:
            f.write(img)
        i += 1
    print("下载完成")

html = get_html_request("http://news.fzu.edu.cn/")
download_jps(html)