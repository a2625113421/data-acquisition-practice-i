import urllib.request
import re

from idna import unichr


def get_html(url):
    try:
        headers = {
            "User-Agent": "Mozilla/5.0 (Windows; U; Windows NT 6.0 x64; en-US; rv:1.9pre) Gecko/2008072421 Minefield/3.0.2pre"}
        req = urllib.request.Request(url, headers=headers)
        data = urllib.request.urlopen(req)
        data = data.read()
        data = data.decode()
        return data
    except Exception as err:
        print(err)

def fill_univ_list(ulist,html):
    while True:
        reg = r'(<div class="ranking" data-v-68e330ae>\s+)(\d+)'
        rank = re.search(reg,html)
        reg = r'(<td data-v-68e330ae>\s+)([\u4e00-\u9fa5]\d+%)'
        level = re.search(reg,html)
        reg = r'(data-v-b80b4d60>)([\u4e00-\u9fa5]+)'
        name = re.search(reg,html)
        reg = r'(<td data-v-68e330ae>\s+)(\d+.\d)'
        ponits = re.search(reg, html)
        if rank != None:
            ulist.append([rank.group(2),level.group(2),name.group(2),ponits.group(2)])
            html = html[name.end():]
        else:
            break

def print_ulist(ulist):
    tplt = "{0:^10}\t{1:{4}^10}\t{2:{4}^10}\t{3:^10}"
    print(tplt.format("2020排名", "全部层次", "学校名称","总分", chr(12288)))
    for u in ulist:
        u[1] = strB2Q(u[1])
        print(tplt.format(u[0], u[1], u[2], u[3], chr(12288)))

def strB2Q(ustring):
    """半角转全角"""
    rstring = ""
    for uchar in ustring:
        inside_code=ord(uchar)
        if inside_code == 32:                                 #半角空格直接转化
            inside_code = 12288
        elif inside_code >= 32 and inside_code <= 126:        #半角字符（除空格）根据关系转化
            inside_code += 65248

        rstring += unichr(inside_code)
    return rstring

url = "https://www.shanghairanking.cn/rankings/bcsr/2020/0812"
ulist = []
html = get_html(url)
fill_univ_list(ulist,html)
print_ulist(ulist)