import requests
from bs4 import BeautifulSoup

import bs4
def get_html(url):
    try:
        headers = {"User-Agent": "Mozilla/5.0 (Windows; U; Windows NT 6.0 x64; en-US; rv:1.9pre) Gecko/2008072421 Minefield/3.0.2pre"}
        r = requests.get(url, timeout = 30, headers=headers)
        r.raise_for_status()
        r.encoding = r.apparent_encoding
    except:
        return "产生异常"
    return r.text

def myFilter(tag):
    return (tag.name=="tbody" and tag.has_attr("id") and tag["id"]=="legend_01_table")

def get_AQI(html):
    AQI_list = []
    soup = BeautifulSoup(html, "lxml")
    for tr in soup.find(myFilter).children:
        if isinstance(tr, bs4.element.Tag):
            for td in tr.find_all("td"):
                AQI_list.append(td.text.strip())
    return AQI_list

def print_AIQ(AQI_list):
    tplt = tplt = "{0:^10}\t{1:{8}^10}\t{2:^10}\t{3:^10}\t{4:^10}\t{5:^10}\t{6:^10}\t{7:{8}^10}"
    print(tplt.format("序号", "城市", "AQI", "PM2.5", "SO2", "NO2", "CO", "首要污染物", chr(12288)))
    for i in range(int(len(AQI_list)/9)):
        print(tplt.format(i+1, AQI_list[i*9], AQI_list[i*9+1], AQI_list[i*9+2], AQI_list[i*9+4],
                          AQI_list[i*9+5], AQI_list[i*9+6], AQI_list[i*9+8], chr(12288)))

html = get_html("https://datacenter.mee.gov.cn/aqiweb2/")
list = get_AQI(html)
print_AIQ(list)